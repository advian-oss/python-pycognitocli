#!/bin/bash -l
set -e
if [ "$#" -eq 0 ]; then
  pycognitocli --help
  pycognitocli -p foo -a bar token get --help
else
  exec "$@"
fi
