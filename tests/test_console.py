"""Test CLI scripts"""
import asyncio
import logging

import pytest
from libadvian.binpackers import ensure_str

from pycognitocli import __version__


LOGGER = logging.getLogger(__name__)


@pytest.mark.asyncio
async def test_version_cli():  # type: ignore
    """Test the CLI parsing for default config dumping works"""
    cmd = "pycognitocli --version"
    process = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    out = await asyncio.wait_for(process.communicate(), 10)
    # Demand clean exit
    assert process.returncode == 0
    # Check output
    assert ensure_str(out[0]).strip().endswith(__version__)


@pytest.mark.asyncio
async def test_help_cli():  # type: ignore
    """Test the CLI parsing for default config dumping works"""
    cmd = "pycognitocli --help"
    process = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    out = await asyncio.wait_for(process.communicate(), 10)
    LOGGER.debug("out={}".format(out))
    # Demand clean exit
    assert process.returncode == 0
    # Check output
    outstr = ensure_str(out[0]).strip()
    assert "-cs, --clientsecret" in outstr
    assert "-a, --appid" in outstr
    assert "token  Token commands" in outstr


@pytest.mark.asyncio
async def test_token_help_cli():  # type: ignore
    """Test the CLI parsing for default config dumping works"""
    cmd = "pycognitocli -p 'fake' -a 'fake' token --help"
    process = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    out = await asyncio.wait_for(process.communicate(), 10)
    LOGGER.debug("out={}".format(out))
    # Demand clean exit
    assert process.returncode == 0
    # Check output
    outstr = ensure_str(out[0]).strip()
    assert "Token commands" in outstr
    assert "get  Get auth token" in outstr


@pytest.mark.asyncio
async def test_token_help_get_cli():  # type: ignore
    """Test the CLI parsing for default config dumping works"""
    cmd = "pycognitocli -p 'fake' -a 'fake' token get --help"
    process = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    out = await asyncio.wait_for(process.communicate(), 10)
    LOGGER.debug("out={}".format(out))
    # Demand clean exit
    assert process.returncode == 0
    # Check output
    outstr = ensure_str(out[0]).strip()
    assert "Get auth token" in outstr
    assert "-c, --curl            Output authorization header for curl" in outstr
